<?php
use Intervention\Image\ImageManagerStatic as Image;
use App\Models\Blog;
use App\Models\Gallery;
use App\Models\Nav;
use App\Models\Prestasi;
use App\Models\Notifikation as notif;
$glr = '';
function navActive($uri, $output='active') {
    $url = explode("/", Request::path());
    if(is_array($uri)) {
      foreach($uri as $u) {
        if (Request::path() == "/" and $u == "/") {
          return $output;
        }
        if (count($url) > 1) {
          if($url[1] == $u) {
            return $output;
          }
        } else {
          if($url[0] == $u) {
            return $output;
          }
        }
      }
    } else {
      if (@$url[1] == $uri) {
        return $output;
      }
    }
  }
function set_active($uri, $output='active') {
  $url = explode("/", Request::path());
  if ($url[0] == $uri || @$url[1] == $uri) {
    return $output;
  } elseif (Request::path() == "mhs" || Request::path() == "/" || $url[0] == '/' and $uri == 'id') {
    return $output;
  }
}
function flagActive() {
    $outputId = 'ind.svg';
    $outputEn = 'uk.svg';
    $url = explode("/", Request::path());
    if ($url[0] == 'en' || @$url[1] == 'en') {
      return $outputEn;
    } elseif (Request::path() == "mhs" || Request::path() == "/" || $url[0] == '/' || $url[0] == 'id' || $url[1] == 'id') {
      return $outputId;
    }
}
function bhsActive() {
    $outputId = 'Indonesia';
    $outputEn = 'English';
    $url = explode("/", Request::path());
    if ($url[0] == 'en' || @$url[1] == 'en') {
      return $outputEn;
    } elseif (Request::path() == "mhs" || Request::path() == "/" || $url[0] == '/' || $url[0] == 'id' || $url[1] == 'id') {
      return $outputId;
    }
}
function navs($bhs) {
  if ($bhs == 'id') {
      $bhsa = 0;
      $jdl = 'judul';
  } elseif ($bhs == 'en') {
      $bhsa = 1;
      $jdl = 'judul_en';
  } else {
      return redirect('mhs/id');
  }
  $navs = Nav::where('bahasa', $bhsa)->get()->toArray();
  return $navs;
}
function getContent($message,$ktgr,&$glr) {
    $dom = new \DomDocument();
    libxml_use_internal_errors(true);
    $dom->loadHtml($message, LIBXML_HTML_NOIMPLIED | LIBXML_HTML_NODEFDTD);
    $images = $dom->getElementsByTagName('img');

    // foreach <img> in the submited message
    $i = 0;
    foreach($images as $img){
      $src = $img->getAttribute('src');

      // if the img source is 'data-url'
      if(preg_match('/data:image/', $src)){

        // get the mimetype
        preg_match('/data:image\/(?<mime>.*?)\;/', $src, $groups);
        $mimetype = $groups['mime'];

        // Generating a random filename
        $filename = uniqid();
        $filepath = "assets/img/blogs/$filename.$mimetype";
        $sImage = Gallery::create(['gambar'=>$filename.".".$mimetype,'kategori'=>$ktgr]);
        $vImage = Gallery::orderBy('id_gallery','desc')->limit('1')->get();
        $glr[$i] = $vImage[0]['id_gallery'];
        $i++;
        // @see http://image.intervention.io/api/
        $image = Image::make($src)
          // resize if required
          /* ->resize(300, 200) */
          ->encode($mimetype, 100) 	// encode file to the specified mimetype
          ->save(public_path($filepath));

        $new_src = asset($filepath);
        $img->removeAttribute('src');
        $img->setAttribute('src', "".$new_src);
      } // <!--endif
    } // <!--endforeach
    $glr = implode(",",(array)$glr);
    return $dom->saveHTML();
    }

    function Insert($judul,$content,$vImage,$request,$bhs,$aktif) {
        $cont = getContent($content,$request->kategori,$glr);

        $create = Blog::create(['judul'=>$judul, 'slug'=> str_slug($judul),
                      'content'=>$cont,'kategori'=>$request->kategori,
                      'bahasa'=>$bhs,'img'=>$vImage[0]['id_gallery'],'imgContent'=>$glr,
                      'view'=>'0','aktif'=>$aktif,'from'=>$request->nim,'baca'=>$aktif,'tanggal'=>$request->date]);
        return $create;
    }
    function InsertPres($judul,$content,$vImage,$request,$bhs,$bc) {
        $cont = getContent($content,'K0prts',$glr);
        $create = Prestasi::create(['judul'=>$judul, 'slug'=> str_slug($judul), 'peserta'=>$request->peserta,
                      'sifat'=>$request->sifat, 'jenjang'=>$request->jenjang, 'content'=>$cont, 'kategori'=>'K0prts',

                      'bhs'=>$bhs, 'tanggal'=>$request->tanggal, 'img'=>$vImage[0]['id_gallery'],
                      'imgDesc'=>$glr,'view'=>'0','aktif'=>'0','from'=>$request->nim,'baca'=>$bc]);
        return $create;
    }
    function Notif() {
      $baca = notif::where('baca','0')->count();
      return $baca;
    }
    function cBlog() {
      $bacaB = Blog::where('baca','0')->count();
      $bacaP = Prestasi::where('baca','0')->count();
      $baca = $bacaB+$bacaP;
      return $baca;
    }
    function rBlog() {
      $baca = Blog::where('baca','0')->orderBy('updated_at','desc')->limit(5)->get();
      return $baca;
    }
    function rPres() {
      $baca = Prestasi::where('baca','0')->orderBy('updated_at','desc')->limit(5)->get();
      return $baca;
    }
?>
