<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use App\Models\Blog;
use App\Models\Kategori;
use App\Models\Gallery;
use App\Models\Prestasi;
use App\Models\Slider;
use App\Models\Event;
use App\Models\Notifikation as notif;
use Illuminate\Support\Str;
use Intervention\Image\ImageManagerStatic as Image;
use Illuminate\Support\Facades\Session;

class AdminController extends Controller {
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function home() {
        return view('admin/home');
    }

    public function SemuaPost(Request $request) {
        //dd($bhs." ".$ktgr);
        if($request->has('tombol')) {
            Session::put('bahasa', $request->input('bahasa'));
            $bhs = Session::get('bahasa');
            //dd($bhs);
        } else {
            if(Session::has('bahasa')) {
                $bhs = Session::get('bahasa');
            } else { $bhs = ''; }
        }
        if($request->has('tombol')) { Session::put('kategori', $request->input('kategori')); $ktgr = Session::get('kategori'); } else { if(Session::has('kategori')) {$ktgr = Session::get('kategori'); } else {$ktgr = ''; } }
        //dd($bhs." ".$ktgr);
        $tampil = Blog::join('kategories', 'blogs.kategori', '=', 'kategories.id_kategori')
                  ->where(function($query) use ($bhs) {
                      if ($bhs >= 0) {
                          $query->where('bahasa',$bhs);
                      } else { $query; }
                  })->where(function($query) use ($ktgr) {
                      if ($ktgr) {
                          $query->where('kategori',$ktgr);
                      } else { $query; }
                  })->where('aktif','1')->orderBy('blogs.created_at','desc')->paginate(5);
        $category = Kategori::where('id_kategori','!=','K0prts')->get();
//dd($tampil);
        if ($request->ajax()) {
            dd($request->has('tombol')." ".$ktgr);
            return view('admin/data-all-post', compact(['tampil','bhs','ktgr']));
        }
        return view('admin/all-post', compact(['tampil','category']));
    }
    public function TambahPost(Request $request) {
        //$cekGambar = Validator::make(, ['file'=>'required|image|mimes|:png,jpeg,jpg,gif']);
        if ($request->hasFile('gBlog')) {
            request()->validate(['gBlog' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',]);
            $gbr = $request->file('gBlog');
            $gambar = time().'.'.request()->gBlog->getClientOriginalExtension();
            $gbr->move(public_path('assets/img/blogs'),$gambar);

        } else {

            //dd($request->hasFile('gambar'));
        }

        $sImage = Gallery::create(['gambar'=>$gambar,'kategori'=>$request->kategori]);
        $vImage = Gallery::orderBy('id_gallery','desc')->limit('1')->get();

        if (Insert($request->judulId,$request->contentId,$vImage,$request,'0','1') and Insert($request->judulEn,$request->contentEn,$vImage,$request,'1',1)) {
            return redirect('/adminn/all-post')->with('success','Post Berhasil ditambahkan');
        }
    }
    public function EditPost($id) {
        $read = Blog::find($id)->update(['baca'=>'1']);
        $blog = Blog::join('galleries', 'blogs.img', '=', 'galleries.id_gallery')->join('kategories', 'blogs.kategori', '=', 'kategories.id_kategori')->where('blogs.id', $id)->get();
        $ktgr = Kategori::where('id_kategori','!=', $blog[0]['kategori'])->get();
        return view('admin/edit-post', compact('blog','ktgr'));
    }
    public function sEditPost(Request $request) {
        //dd($request->toArray());
        if (empty($request->file('gambarPost'))) {
            $vImage = Gallery::join('blogs', 'blogs.img', '=', 'galleries.id_gallery')->where('blogs.id',$request->data)->get();
            //dd($vImage[0]['id_gallery']);
        } else {
            $this->validate($request, ['gambarPost' => 'required|file|image|mimes:jpeg,png,jpg,gif,svg|max:2048',]);
            $gbr = $request->file('gambarPost');
            $gambar = time().'.'.request()->gambarPost->getClientOriginalExtension();
            $gbr->move(public_path('assets/img/blogs'),$gambar);

            $sImage = Gallery::create(['gambar'=>$gambar,'kategori'=>$request->kategori]);
            $vImage = Gallery::orderBy('created_at','desc')->limit('1')->get()->toArray();
        }
        $cont = getContent($request->content,$request->kategori,$glr);
        if (Blog::find($request->data)->update(['judul'=>$request->judul,'slug'=>str_slug($request->judul),'content'=>$cont,'imgContent'=>$glr,'kategori'=>$request->kategori,'img'=>$vImage[0]['id_gallery'],'tanggal'=>$request->tanggal])) {
            return back();
        }
    }
    public function SetujuPost($page,Request $request) {
        if ($page == "post") {
            $tampil = Blog::join('kategories', 'blogs.kategori', '=', 'kategories.id_kategori')->where('aktif','!=','1')->orderBy('blogs.updated_at','desc')->paginate(5);
            $read = Blog::where('baca', '!=','1')->update(['baca'=>'1']);
        } else {
            $tampil = prestasi::join('kategories', 'prestasi.kategori', '=', 'kategories.id_kategori')->where('aktif','!=','1')->orderBy('prestasi.updated_at','desc')->paginate(5);
            $read = Prestasi::where('baca', '!=','1')->update(['baca'=>'1']);
        }
        if ($request->ajax()) {
            return view('admin/data-setuju-'.$page, compact(['tampil']));
        }
        return view('admin/setuju-'.$page, compact(['tampil','category']));
    }

    public function Setujukan($data) {
        if ($pe == '0') {
            $pes = "Diterima";
        } elseif($pe == "2") {
            $pes = "berhasil diedit";
        }
        if ($page=="post") {
            $blog = Blog::where('id', $data)->get(); $pe = $blog[0]['aktif'];
            $conf = Prestasi::find($data)->update(['aktif' => '1']);
            $tampil = Blog::join('kategories', 'blogs.kategori', '=', 'kategories.id_kategori')->where('aktif','0')->paginate(5);
        } else {
            $blog = Prestasi::where('id', $data)->get(); $pe = $blog[0]['aktif'];
            $conf = Prestasi::find($data)->update(['aktif' => '1']);
            $tampil = Prestasi::join('kategories', 'prestasi.kategori', '=', 'kategories.id_kategori')->where('aktif','0')->paginate(5);
        }
        $pesan = "Selamat Post Anda Yang Berjudul <b>".$blog[0]['judul']."</b> ".$pes.", Silahkan Cek di Website";
        $notif = notif::create(['pesan'=> $pesan,'kd_content'=>$blog[0]['id'],'to'=>'16650063','baca'=>'0']);

        return back()->with('success','Selamat berhasil disetujui');
    }

    public function DestroyPost(Request $request) {
        if (Blog::find($request->hapus)->delete()) {
            $tampil = Blog::join('kategories', 'blogs.kategori', '=', 'kategories.id_kategori')->where('aktif',$request->hps)->orderBy('blogs.created_at','desc')->paginate(5);
            $category = Kategori::where('id_kategori', '!=','K0prt');
                if ($request->ajax()) {
                    return view('admin/data-setuju-post', compact(['tampil']));
                }

        } else {
            dd('agus'.$request->hapus);
        }
    }
    public function HapusPost($id) {
        Blog::find($id)->delete();
        $tampil = Blog::join('kategories', 'blogs.kategori', '=', 'kategories.id_kategori')->where('aktif','1')->orderBy('blogs.created_at','desc')->paginate(5);
        $category = Kategori::where('id_kategori', '!=','K0prt');
        return redirect('/adminn/all-post')->with('success', 'Post berhasil dihapus');
    }
    public function SliderPost(Request $request) {
      $tampil = Gallery::join('sliders', 'sliders.id_gambar', '=', 'galleries.id_gallery')->paginate(5);
     // dd($tampil->toArray());
      if ($request->ajax()) {
          return view('admin/data-slider-post', compact(['tampil']));
      }
      return view('admin/slider-post', compact(['tampil']));
    }
    public function TambahSlider(Request $request) {
        $this->validate($request, ['gambarCaption' => 'required|file|image|mimes:jpeg,png,jpg,gif,svg|max:2048',]);
        $gbr = $request->file('gambarCaption');
        $gambar = time().'.'.request()->gambarCaption->getClientOriginalExtension();
        $gbr->move(public_path('assets/img/blogs'),$gambar);

        $sImage = Gallery::create(['gambar'=>$gambar,'kategori'=>'K0sldr']);
        $vImage = Gallery::orderBy('created_at','desc')->limit('1')->get()->toArray();
        if (Slider::create(['id_gambar'=>$vImage[0]['id_gallery'],'captionId'=>$request->captionId,'captionEn'=>$request->captionEn])) {
            return redirect('/adminn/all-slider');
        }
        //dd(getContent($request->contentId)."<br />".getContent($request->contentEn));
    }
    public function EditSlider($id) {
      $tampil = Gallery::join('sliders', 'sliders.id_gambar', '=', 'galleries.id_gallery')->where('sliders.id',$id)->get();
      return view('admin/edit-slider', compact(['tampil']));
    }
    public function sEditSlider(Request $request) {
        if (empty($request->file('gambarCaption'))) {
            $vImage = Gallery::join('sliders', 'sliders.id_gambar', '=', 'galleries.id_gallery')->where('sliders.id',$request->data)->get();
            //dd($vImage[0]['id_gallery']);
        } else {

            //$this->validate($request, ['gambarCaption' => 'required|file|image|max:2048|mimes:jpeg,png,jpg,gif,svg',]);
            $gbr = $request->file('gambarCaption');
            $gambar = time().'.'.request()->gambarCaption->getClientOriginalExtension();

            $gbr->move(public_path('assets/img/blogs'),$gambar);

            $sImage = Gallery::create(['gambar'=>$gambar]);
            $vImage = Gallery::orderBy('created_at','desc')->limit('1')->get()->toArray();
        }
        if (Slider::find($request->data)->update(['id_gambar'=>$vImage[0]['id_gallery'],'captionId'=>$request->captionId,'captionEn'=>$request->captionEn])) {
            return redirect('/adminn/all-slider');
        }
    }
    public function DestroySlider(Request $request) {
        Slider::find($request->hapus)->delete();
        $tampil = Gallery::join('sliders', 'sliders.id_gambar', '=', 'galleries.id_gallery')->paginate(5);
        if ($request->ajax()) {
            return view('admin/data-slider-post', compact(['tampil']));
        }
    }
    public function Prestasi(Request $request) {
        $tampil = Prestasi::orderBy('created_at', 'desc')->paginate(5);
        if ($request->ajax()) {
            return view('admin/data-prestasi', compact(['tampil']));
        }
        return view('admin/prestasi', compact(['tampil']));
    }
    public function sPrestasi(Request $request) {
        //dd($request->toArray());
        if ($request->hasFile('gBlog')) {
            request()->validate(['gBlog' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',]);
            $gbr = $request->file('gBlog');
            $gambar = time().'.'.request()->gBlog->getClientOriginalExtension();
            $gbr->move(public_path('assets/img/blogs'),$gambar);
        } else { }
            //dd($request->hasFile('gambar'));
        $bhs = $request->bhs;
        $navs = navs($bhs);
        $sImage = Gallery::create(['gambar'=>$gambar,'kategori'=>'prestasi']);
        $vImage = Gallery::orderBy('id_gallery','desc')->limit('1')->get();

        if (InsertPres($request->judul,$request->descId,$vImage,$request,'0','1') and InsertPres($request->lombaEn,$request->descEn,$vImage,$request,'1','1')) {
            return redirect('/adminn/all-prestasi')->with(compact(['navs','bhs']))->with('success','Post Berhasil ditambahkan');
        }
    }
    public function DestroyPrestasi(Request $request) {
        $destroy = Prestasi::find($request->data)->delete();
        $tampil = Prestasi::orderBy('created_at', 'desc')->paginate(5);
        if ($request->ajax()) {
            return view('admin/data-prestasi', compact(['tampil']));
        }
    }
    public function EditPrestasi($id) {
        $tampil = Prestasi::join('galleries','galleries.id_gallery','prestasi.img')->where('prestasi.id',$id)->get();
        //dd($tampil);
        return view('admin/edit-prestasi', compact(['tampil']));
    }
    public function sEditPrestasi(Request $request) {
        $cont = getContent($request->content,'K0prts',$glr);
        if ($request->hasFile('gBlog')) {
            request()->validate(['gBlog' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',]);
            $gbr = $request->file('gBlog');
            $gambar = time().'.'.request()->gBlog->getClientOriginalExtension();
            $gbr->move(public_path('assets/img/blogs'),$gambar);
        } else {
            $tampil = Prestasi::find($request->data);
            $gambar = $tampil->img;
        }
        $update = Prestasi::find($request->data)->update(['judul'=>$request->judul,
        'slug'=>str_slug($request->judul),'peserta'=>$request->peserta,'sifat'=>$request->sifat,
        'jenjang'=>$request->jenjang,'content'=>$cont,'tanggal'=>date('Y-m-d', strtotime($request->tanggal)),
        'img'=>$gambar,'imgDesc'=>$glr]);
        $tampil = Prestasi::orderBy('created_at', 'desc')->paginate(5);
        if ($update) {
            return redirect('/adminn/all-prestasi')->with('success','Agenda Berhasil diupdate');
        }
    }
}
