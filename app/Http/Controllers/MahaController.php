<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use App\Models\Blog;
use App\Models\Kategori;
use App\Models\Nav;
use App\Models\Gallery;
use App\Models\Prestasi;
use App\Models\Notifikation as notif;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Session;
use Yajra\Datatables\Datatables;

class MahaController extends Controller
{
  public function __construct()
  {
      $this->middleware('auth');
  }
    public function index($bhs = 'id') {
        $navs = navs($bhs);
        $maha = 'mhs';
        //$kategori = kategori::all();
        return view('mhs/home', compact(['maha','bhs','navs','jdl']));
    }
    public function allPost($bhs='id') {
        $maha = 'mhs';
        $category = Kategori::where('id_kategori','!=','K0prts')->get();
        $navs = navs($bhs);
        return view('mhs/all-post', compact(['maha','bhs','navs','category']));
    }
    public function addPrestasi($bhs='id') {
        $maha = 'mhs';
        $category = Kategori::all();
        $navs = navs($bhs);
        return view('mhs/prestasi', compact(['maha','bhs','navs','category']));
    }
    public function Prestasi(Request $request,$bhs="id") {
      $navs = navs($bhs);
        $tampil = Prestasi::orderBy('created_at', 'desc')->paginate(5);
        if ($request->ajax()) {
            return view('mhs/data-prestasi', compact(['tampil']));
        }
        return view('mhs/all-prestasi', compact(['bhs','navs','tampil']));
    }
    public function editPrestasi($bhs, $id) {
        $tampil = Prestasi::join('galleries','galleries.id_gallery','prestasi.img')->where([['prestasi.id',$id],['prestasi.from','16650063']])->get();
        $navs = navs($bhs);
        return view('mhs/edit-prestasi', compact(['tampil','bhs','navs']));
    }
    public function sEditPrestasi(Request $request) {
        $navs = navs($request->bhs);
        $cont = getContent($request->content,'K0prts',$glr);
        if ($request->hasFile('gBlog')) {
            request()->validate(['gBlog' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',]);
            $gbr = $request->file('gBlog');
            $gambar = time().'.'.request()->gBlog->getClientOriginalExtension();
            $gbr->move(public_path('assets/img/blogs'),$gambar);
        } else {
            $tampil = Prestasi::find($request->data);
            $gambar = $tampil->img;
        }
        $update = Prestasi::find($request->data)->update(['judul'=>$request->judul,
        'slug'=>str_slug($request->judul),'peserta'=>$request->peserta,'sifat'=>$request->sifat,
        'jenjang'=>$request->jenjang,'content'=>$cont,'tanggal'=>date('Y-m-d', strtotime($request->tanggal)),
        'img'=>$gambar,'imgDesc'=>$glr]);
        $tampil = Prestasi::orderBy('created_at', 'desc')->paginate(5);
        if ($update) {
            return redirect('/'.$request->bhs.'/mhs/all-prestasi')->with('success','Prestasi Berhasil diupdate');
        }
    }
    public function sPrestasi(Request $request) {
        if ($request->hasFile('gBlog')) {
            request()->validate(['gBlog' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',]);
            $gbr = $request->file('gBlog');
            $gambar = time().'.'.request()->gBlog->getClientOriginalExtension();
            $gbr->move(public_path('assets/img/blogs'),$gambar);
        } else {
            //dd($request->hasFile('gambar'));
        }
        $bhs = $request->bhs;
        $navs = navs($bhs);
        $sImage = Gallery::create(['gambar'=>$gambar,'kategori'=>'prestasi']);
        $vImage = Gallery::orderBy('id_gallery','desc')->limit('1')->get();

        if (InsertPres($request->lombaId,$request->descId,$vImage,$request,'0','0') and InsertPres($request->lombaEn,$request->descEn,$vImage,$request,'1','0')) {
            return redirect('/'.$bhs.'/mhs/prestasi')->with(compact(['navs','bhs']))->with('success','Post Berhasil ditambahkan');
        }
    }
    public function hPrestasi(Request $request) {
        $destroy = Prestasi::find($request->data)->delete();
        $tampil = Prestasi::orderBy('created_at', 'desc')->paginate(5);
        if ($request->ajax()) {
            return view('mhs/data-prestasi', compact(['tampil']));
        }
    }
    public function dataAllPost($data='mhs',Request $request) {
        if($data == 'mhs') {
            $wh = 'from'; $val = '16650063';
        } else {
            $wh = 'aktif'; $val = '1';
        }
        $post = Kategori::join('blogs', 'blogs.kategori', '=', 'kategories.id_kategori')
        ->orderBy('blogs.id','desc')->where([[$wh,$val],['blogs.deleted_at', null]])->get();
        $tampil =  Datatables::of($post)
        ->filter(function ($instance) use ($request) {
            if ($request->has('kategori')) {
                $instance->collection = $instance->collection->filter(function ($row) use ($request) {
                    return Str::contains($row['id_kategori'], $request->get('kategori')) ? true : false;
                });
            }
            if ($request->has('bahasa')) {
                $instance->collection = $instance->collection->filter(function ($row) use ($request) {
                    return Str::contains($row['bahasa'], $request->get('bahasa')) ? true : false;
                });
            }
        })->make(true);

        return $tampil;
    }
    public function addPost($bhs = 0) {
        $navs = navs($bhs);
        $maha = 'mhs';
        //$kategori = kategori::all();
        return view('mhs/add-post', compact(['maha','bhs','navs','jdl']));
    }
    public function sAddPost(Request $request) {
        if ($request->hasFile('gBlog')) {
            request()->validate(['gBlog' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',]);
            $gbr = $request->file('gBlog');
            $gambar = time().'.'.request()->gBlog->getClientOriginalExtension();
            $gbr->move(public_path('assets/img/blogs'),$gambar);

        } else {

            //dd($request->hasFile('gambar'));
        }
        $bhs = $request->bhs;
        $navs = navs($bhs);
        $sImage = Gallery::create(['gambar'=>$gambar,'kategori'=>$request->kategori]);
        $vImage = Gallery::orderBy('id_gallery','desc')->limit('1')->get();

        if (Insert($request->judulId,$request->contentId,$vImage,$request,'0','0') and Insert($request->judulEn,$request->contentEn,$vImage,$request,'1','0')) {
            return redirect('/'.$bhs.'/mhs/all-post')->with(compact(['navs','bhs']))->with('success','Post Berhasil ditambahkan');
        }
    }
    public function editPost($bhs, $id) {
        $navs = navs($bhs);
        $maha = 'mhs';
        $blog = Blog::join('galleries', 'blogs.img', '=', 'galleries.id_gallery')->join('kategories', 'blogs.kategori', '=', 'kategories.id_kategori')->where('blogs.id', $id)->get();
        $ktgr = Kategori::where('id_kategori','!=', $blog[0]['kategori'])->get();
        return view('mhs/edit-post', compact('maha','navs','bhs','blog','ktgr'));

    }
    public function sEditPost(Request $request) {
        if (empty($request->file('gambarPost'))) {
            $vImage = Gallery::join('blogs', 'blogs.img', '=', 'galleries.id_gallery')->where('blogs.id',$request->data)->get();
            //dd($vImage[0]['id_gallery']);
        } else {
            $this->validate($request, ['gambarPost' => 'required|file|image|mimes:jpeg,png,jpg,gif,svg|max:2048',]);
            $gbr = $request->file('gambarPost');
            $gambar = time().'.'.request()->gambarPost->getClientOriginalExtension();
            $gbr->move(public_path('assets/img/blogs'),$gambar);

            $sImage = Gallery::create(['gambar'=>$gambar,'kategori'=>$request->kategori]);
            $vImage = Gallery::orderBy('created_at','desc')->limit('1')->get()->toArray();
        }
        $cont = getContent($request->content,$request->kategori,$glr);
        if (Blog::find($request->data)->update(['judul'=>$request->judul,'slug'=>str_slug($request->judul),'content'=>$cont,'imgContent'=>$glr,'kategori'=>$request->kategori,'tanggal'=>$request->date,'img'=>$vImage[0]['id_gallery'],'aktif'=>'2','baca'=>'0'])) {
            return back();
        }
    }
    public function DestroyPost($bhs, $hapus) {
        if (Blog::find($hapus)->delete()) {
            $navs = navs($bhs);
            return redirect('/'.$bhs.'/mhs/all-post')->with(compact(['navs','bhs']));
        }
    }
    public function notifikasi(Request $request,$bhs =0) {
        $read = notif::where('baca', '!=','1')->update(['baca'=>'1']);
        $baca = notif::orderBy('updated_at','desc')->paginate(5);
        $navs = navs($bhs);
        if ($request->ajax()) {
            return view('mhs/data-notif', compact(['baca']));
        }
        return view('mhs/notif', compact(['navs','bhs','baca']));
    }
}
