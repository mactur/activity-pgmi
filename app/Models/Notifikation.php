<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Notifikation extends Model
{
    protected $table = 'notifikations';
    protected $guarded = [];
}
