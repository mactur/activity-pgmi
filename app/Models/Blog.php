<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use DB;
use Laravel\Scout\Searchable;

class Blog extends Model {

    use Searchable;
    protected $table = 'blogs';
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $guarded = [];

    public function gBlog() {
        return $this->belongsToMany('App\Models\Gallery', 'id_gallery','img');
    }
    public function kBlog() {
        return $this->belongsToMany('App\Models\Kategori', 'id_kategori','kategori');
    }
    public function toSearchableArray() {
        $array = $this->toArray();

        // Customize array...

        return $array;
    }
}
