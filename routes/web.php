<?php
Auth::routes();

//Route::get('/home', 'HomeController@index')->name('home');
Route::get('/adminn', 'AdminController@home')->middleware('auth');
Route::get('/adminn/all-post', array('as'=>'all', 'uses'=>'AdminController@SemuaPost'))->middleware('auth');
Route::view('/adminn/add-post', 'admin.add-post')->middleware('auth');
Route::get('/adminn/edit-post/{id}', array('as'=>'editPost', 'uses'=>'AdminController@EditPost'))->middleware('auth');
Route::post('/adminn/edit-post/', array('as'=>'editPost', 'uses'=>'AdminController@sEditPost'))->middleware('auth');
Route::post('/adminn/add-post', array('as'=>'SimpanPost', 'uses'=>'AdminController@TambahPost'))->middleware('auth');
Route::delete('/adminn/destroy-post/', array('as'=>'HapusPost', 'uses'=>'AdminController@DestroyPost'))->middleware('auth');
Route::get('/adminn/hapus-post/{data}', array('as'=>'hPost', 'uses'=>'AdminController@HapusPost'))->middleware('auth');
Route::get('/adminn/confirm-post/{page}', array('as'=>'SetujuPost', 'uses'=>'AdminController@SetujuPost'))->middleware('auth');
Route::get('/adminn/confirmed/{data?}', array('as'=>'Setujui', 'uses'=>'AdminController@Setujukan'))->middleware('auth');
Route::get('/adminn/all-slider', array('as'=>'slider', 'uses'=>'AdminController@SliderPost'))->middleware('auth');
Route::view('/adminn/add-slider', 'admin.add-slider')->middleware('auth');
Route::post('/adminn/add-slider', array('as'=>'SimpanSlider', 'uses'=>'AdminController@TambahSlider'))->middleware('auth');
Route::get('/adminn/edit-slider/{data}', array('as'=>'eSlider', 'uses'=>'AdminController@EditSlider'))->middleware('auth');
Route::post('/adminn/edit-slider', array('as'=>'eSlider', 'uses'=>'AdminController@sEditSlider'))->middleware('auth');
Route::delete('/adminn/destroy-slider', array('as'=>'HapusSlider', 'uses'=>'AdminController@DestroySlider'))->middleware('auth');

Route::get('/{bhs?}/mhs', ['as'=>'mhs','uses'=>'MahaController@index']);
Route::get('/{bhs?}/mhs/all-prestasi', ['as'=>'prestasi','uses'=>'MahaController@Prestasi']);
Route::get('/{bhs?}/mhs/prestasi', ['as'=>'prestasi','uses'=>'MahaController@addPrestasi']);
Route::DELETE('/mhs/destroy-prestasi', ['as'=>'dPrestasi','uses'=>'MahaController@hPrestasi']);
Route::get('/{bhs?}/mhs/edit-prestasi/{id}', ['as'=>'ePrestasi','uses'=>'MahaController@editPrestasi']);
Route::PUT('/mhs/edit-prestasi/', ['as'=>'sPrestasi','uses'=>'MahaController@sEditPrestasi']);
Route::post('/mhs/add-prestasi', ['as'=>'add-prestasi','uses'=>'MahaController@sPrestasi']);
Route::get('/{bhs?}/mhs/all-post', ['as'=>'mhsAll', 'uses'=>'MahaController@allPost']);
Route::get('/dataAll-post/{data?}', ['as'=>'mhsData', 'uses'=>'MahaController@dataAllPost']);
Route::get('/{bhs?}/mhs/add-post', ['as'=>'mhsAdd', 'uses'=>'MahaController@addPost']);
Route::post('/mhs/add-post', ['as'=>'sAdd', 'uses'=>'MahaController@sAddPost']);
Route::get('/{bhs?}/mhs/edit-post/{data}', ['as'=>'mhsEdit', 'uses'=>'MahaController@editPost']);
Route::post('/mhs/edit-post/', ['as'=>'sEdit', 'uses'=>'MahaController@sEditPost']);
Route::get('/{bhs?}/mhs/destroy-post/{data}', array('as'=>'hPost', 'uses'=>'MahaController@DestroyPost'));
Route::get('/{bhs?}/mhs/notif', ['as'=>'notif','uses'=>'MahaController@notifikasi']);

Route::get('/adminn/all-prestasi', array('as'=>'Prestasi', 'uses'=>'AdminController@Prestasi'))->middleware('auth');
Route::view('/adminn/add-prestasi', 'admin.add-prestasi')->middleware('auth');
Route::post('/adminn/add-prestasi', array('as'=>'addPrestasi', 'uses'=>'AdminController@sPrestasi'))->middleware('auth');
Route::delete('/adminn/destroy-prestasi', array('as'=>'HapusPrestasi', 'uses'=>'AdminController@DestroyPrestasi'))->middleware('auth');
Route::get('/adminn/edit-prestasi/{data}', 'AdminController@EditPrestasi')->middleware('auth');
Route::PUT('/adminn/edit-prestasi/', ['as'=>'ePrestasi', 'uses'=>'AdminController@sEditPrestasi'])->middleware('auth');

//Route::get('/{bhs?}/login', 'BlogController@login');
Route::get('/{bhs?}/login/mhs', 'BlogController@masuk');
Route::get('/{bhs?}/gallery', array('as'=>'gallery','uses'=>'BlogController@gallery'));
Route::get('{bhs?}/cara-upload/', 'BlogController@guide');
//hmps,guide
Route::get('/{bhs?}/result', 'BlogController@search');
Route::get("/{bhs?}/{hmps}",array('as'=>'blogs','uses'=>'BlogController@blogs'));
Route::get("/{bhs?}/{page='cara upload'}",array('as'=>'blogs','uses'=>'BlogController@blogs'));
//kegiatan,ekstra
Route::get('{bhs?}/Prestasi/{slug}', 'BlogController@prestasi');
Route::get('/{bhs?}/kegiatan/{page}',array('as'=>'blogs','uses'=>'BlogController@blogs'));
Route::get('/{bhs?}/Kegiatan/{slug}', 'BlogController@post');
Route::get('/{bhs?}/ekstra/{page}',array('as'=>'blogs','uses'=>'BlogController@blogs'));
Route::get('{bhs?}/Ekstra/{slug}', 'BlogController@post');
Route::get('{bhs?}/HMPS/{slug}', 'BlogController@post');
//Route::get("blogs/{page}/{bhs?}", 'BlogController@blogs');

Route::get('/{bhs?}', 'BlogController@index');
