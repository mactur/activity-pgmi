<div class="box-body table-responsive no-padding">
  <table class="table table-bordered table-striped">
    <tr>
      <th>No</th>
      <th>Judul</th>
      <th>Bahasa</th>
      <th>Tanggal Agenda</th>
      <th>Action</th>
    </tr>
    @foreach ($tampil as $tam)
    <tr>
      <td>{{ (($tampil->currentPage() - 1 ) * $tampil->perPage() ) + $loop->iteration }}</td>
      <td>{{$tam->judul}}</td>
      <td>{{$tam->bhs == 0 ? 'Indonesia' : 'English'}}</td>
      <td>{{date('d M Y', strtotime($tam->tanggal))}}</td>
      <td>
        {!! Form::open(['url'=>''])!!}
        {{csrf_field()}}
        <button type="button" onclick="window.location.href='/'{{$bhs}}'/mhs/edit-prestasi/{{$tam->id}}'" class="btn btn-primary btn-edit"><i class="fa fa-pencil" aria-hidden="true"></i></button>
        <input type="hidden" name="_method" value="DELETE">
        <button type="button" onclick="destroy({{$tam->id}})" class="btn btn-danger btn-hapus"><i class="fa fa-trash" aria-hidden="true"></i></button>
        {!!Form::close()!!}
      </td>
    </tr>
    @endforeach
  </table>
</div>
<!-- /.box-body -->
<div class="col-sm-12 pagination-wrap">
{!! $tampil->render() !!}
</div>
</div>
