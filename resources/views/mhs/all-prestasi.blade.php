@section('js')
  <script type="text/javascript">
  function destroy(post) {
    if (confirm('yakin ingin dihapus?')) {
      $.ajax({
          url : '/mhs/destroy-prestasi/',
          type : 'post',
          data : {'data': post,'_method':$('input[name=_method]').val(),'_token':$('input[name=_token]').val()},
          success: function(data) {
              $('.post-all').empty().html(data);
              $('.sukses').show();
              $('#message').html('Post Berhasil dihapus');
          }
      });
    }
  }
  </script>
@stop
@extends('mhs.maha')
@section('title', 'Prestasi Mahasiswa')
@section('content')

            <h2 style="margin-top:0" id="selamat-mhs">PRESTASI POST</h2>
            <div class="underscore" style="margin-left:0px;margin-left:0px;margin-bottom:15px;"></div>
            <div class="box-tools pull-right" style="margin-bottom:10px">
              <div class="input-group input-group-sm" style="width: 150px;">
                <button type="button" onclick="window.location.href='/{{$bhs}}/mhs/prestasi'" class="btn btn-default"><i class="glyphicon glyphicon-plus" style="margin-right:5px;"></i>Tambah Prestasi Post</button>
              </div>
            </div>
          <!-- /.box-header -->
          <div class="col-sm-12 table-responsive" style="padding: 0;">
            <div class="alert alert-success sukses" hidden="hidden">
                <button type="button" class="close" data-dismiss="alert">×</button>
                <strong id="message"></strong>
            </div>
            @if ($message = Session::has('message'))
            <div class="alert alert-success sukses">
                <button type="button" class="close" data-dismiss="alert">×</button>
                    <strong>{{$message}}</strong>
            </div>
            @endif
              <div class="post-all">

              @include('mhs.data-prestasi')
              </div>

          <!-- /.box-body -->
          </div>

@endsection
