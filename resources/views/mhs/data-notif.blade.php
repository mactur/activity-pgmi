<table class="table table-striped table-bordered" style="margin-top:25px;">
    <thead>
        <tr class="title-table-mhs">
        <th>No</th>
        <th>Tanggal</th>
        <th>Pemberitahuan</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($baca as $bc)
        <tr class="success">
        <td>{{ (($baca->currentPage() - 1 ) * $baca->perPage() ) + $loop->iteration }}</td>
        <td>{{date('d M Y', strtotime($bc->created_at))}}</td>
        <td>{!!$bc->pesan!!}</td>
        </tr>
        @endforeach
    </tbody>
    </table>
    <div class="col-sm-12 pagination-wrap">
        {{$baca->render()}}
    </div>