<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>@yield('title')</title>

    <!-- Bootstrap -->
    <link href="{{asset('assets/css/bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{asset('assets/css/aos.css')}}" rel="stylesheet">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.8/summernote.css" rel="stylesheet">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans" type="text/css" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
    <link href="{{asset('assets/css/style.css')}}" rel="stylesheet">
    <link href="{{asset('assets/plugins/datatables/jquery.dataTables.min.css')}}" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Lora:400,400i,700|Montserrat:400,700|Open+Sans:400,400i,700,700i" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/owl-carousel/1.3.3/owl.carousel.min.css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Titillium+Web" type="text/css" />
    <link rel="stylesheet" href="{{asset('assets/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css')}}">
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
    <script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
    <script src="{{asset('assets/js/bootstrap.min.js')}}"></script>
    <script src="{{asset('assets/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js')}}"></script>
    <script src="{{asset('assets/js/aos.js')}}"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/owl-carousel/1.3.3/owl.carousel.min.js"></script>
    <script src="{{asset('assets/js/post-slider.js')}}"></script>
    <script src="{{asset('assets/js/topscroll.js')}}"></script>
    <script src="{{asset('assets/js/search.js')}}"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/baguettebox.js/1.8.1/baguetteBox.min.css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/baguettebox.js/1.8.1/baguetteBox.min.js"></script>
    <link href="https://fonts.googleapis.com/css?family=Lora" rel="stylesheet">
    <script src="{{asset('assets/plugins/datatables/moment.js')}}"></script>
<!-- jQuery UI 1.11.4 -->

    <!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
    <!-- Bootstrap 3.3.6 -->
    <!-- Morris.js charts -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.11.2/moment.min.js"></script>
    <script src="{{asset('assets/plugins/daterangepicker/daterangepicker.js')}}"></script>
    <!-- datepicker -->
    <script src="{{asset('assets/plugins/datepicker/bootstrap-datepicker.js')}}"></script>
    <!-- Bootstrap WYSIHTML5 -->
    <script>
        $.widget.bridge('uibutton', $.ui.button);
    </script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.8/summernote.min.js"></script>
  </head>
  <body>

    @include('layouts.header')
      <div id="wrap-admin-mhs-outer">
        <div id="wrap-admin-mhs-inner" style="padding-top:100px" class="container">
          <div class="col-sm-3">
              <h2 id="selamat-mhs">Mahasiswa</h2>
              <h4 class="menu-mhs-admin-text"><a href="/{{$bhs}}/mhs/notif"><i class="glyphicon glyphicon-bullhorn" style="margin-right:5px"></i>Pemberitahuan<span class="badge" style="background-color:rgb(57, 206, 185);margin-left:5px;">{{Notif() > 0 ? Notif() : ""}}</span></a></h4>
              <h4 class="menu-mhs-admin-text"><a href="/{{$bhs}}/mhs/all-post"><i class="glyphicon glyphicon-file" style="margin-right:5px"></i>Post</a></h4>
              <h4 class="menu-mhs-admin-text"><a href="/{{$bhs}}/mhs/all-prestasi"><i class="glyphicon glyphicon-star" style="margin-right:5px"></i>Prestasi</a></h4>
              <!--<h4 class="menu-mhs-admin-text"><a href="#" onclick="event.preventDefault();
                       document.getElementById('logout-form').submit();><i class="glyphicon glyphicon-log-out" style="margin-right:5px"></i>Logout</a>
                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    {{ csrf_field() }}
                </form>
              </h4>-->
          </div>
          <div class="col-sm-9" style="padding-top:20px">
              @yield('content')
          </div>
        </div>
      </div>
    @include('layouts.footer')
    <div class='scrolltop'>
        <div class='scroll icon'><i class="glyphicon glyphicon-eject"></i></div>
    </div>
    @section('js')
    @show
    <script src="{{asset('assets/js/flag.js')}}"></script>
    <script type="text/javascript">
    $(window).on('hashchange', function() {
        if (window.location.hash) {
            var page = window.location.hash.replace('#', '');
            if (page == Number.NaN || page <= 0) {
                return false;
            } else {
                getData(page);
            }
        }
    });

    $(document).ready(function() {
        $(document).on('click', '.pagination a',function(event) {
            $('li').removeClass('active');
            $(this).parent('li').addClass('active');
            event.preventDefault();

            var myurl = $(this).attr('href');
            var page=$(this).attr('href').split('page=')[1];

            getData(page);
        });
    });

    function getData(page) {
            $.ajax({
                url: '?page=' + page,
                type: "get",
                datatype: "html",
            }).done(function(data) {
                $(".post-all").empty().html(data);
                location.hash = page;
            })
            .fail(function(jqXHR, ajaxOptions, thrownError)
            {
                  alert('No response from server');
            });
    }
    </script>
    <script>
      $(document).ready(function(){

          $(".filter-button").click(function(){
              var value = $(this).attr('data-filter');

              if(value == "all")
              {
                  //$('.filter').removeClass('hidden');
                  $('.filter').show('1000');
              }
              else
              {
      //            $('.filter[filter-item="'+value+'"]').removeClass('hidden');
      //            $(".filter").not('.filter[filter-item="'+value+'"]').addClass('hidden');
                  $(".filter").not('.'+value).hide('3000');
                  $('.filter').filter('.'+value).show('3000');

              }
          });

          if ($(".filter-button").removeClass("active")) {
      $(this).removeClass("active");
      }
      $(this).addClass("active");

      });

    </script>
    <script src="{{asset('assets/js/gallery.js')}}"></script>
    </body>
</html>
