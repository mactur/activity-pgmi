@if ($message = Session::get('success'))
<div class="alert alert-success sukses">
    <button type="button" class="close" data-dismiss="alert">×</button>
        <strong>{{$message}}</strong>
</div>
@endif
<div class="box-body table-responsive no-padding">
  <table class="table table-bordered table-striped">
    <tr>
      <th>No</th>
      <th>Judul</th>
      <th>Tanggal</th>
      <th>Status</th>
      <th>Bahasa</th>
      <th>User</th>
      <th>Action</th>
    </tr>
    @foreach ($tampil as $tam)
    <tr>
      <td>{{ (($tampil->currentPage() - 1 ) * $tampil->perPage() ) + $loop->iteration }}</td>
      <td>{{str_limit(strip_tags($tam->judul), 50) }}</td>
      <td>{{date('d M Y', strtotime($tam->created_at))}}</td>
      <td>@if ($tam->aktif == 0) Post Baru @elseif ($tam->aktif == 2) Post Edit @endif</td>
      <td>{{$tam->bahasa == 0 ? 'Id' : 'En'}}</td>
      <td>{{$tam->from}}</td>
      <td>
        {!! Form::open(['url'=>''])!!}
        {{csrf_field()}}
        <button type="button" name="setuju" data-toggle="tooltip" data-placement="bottom" onClick="confirmed({{$tam->id}})" title="Setuju" class="btn btn-success btn-setuju">
          <i class="fa fa-check-square-o" aria-hidden="true"></i>
        </button>
        <a href="#" data-toggle="tooltip" data-placement="bottom" title="Lihat atau Edit">
            <button type="button" onclick="window.location.href='/adminn/edit-prestasi/{{$tam->id}}'" class="btn btn-primary btn-edit"><i class="fa fa-eye" aria-hidden="true"></i></button>
        </a>
        <input type="hidden" name="_method" value="DELETE">
        <input type="hidden" name="hps" value="0">
        <button type="button" onclick="destroy({{$tam->id}})" class="btn btn-danger btn-hapus" data-toggle="tooltip" data-placement="bottom" title="Hapus"><i class="fa fa-trash" aria-hidden="true"></i></button>
        {!!Form::close()!!}
      </td>
    </tr>
    @endforeach
  </table>
</div>
<div class="col-sm-12 pagination-wrap">
    {!! $tampil->render() !!}
</div>
