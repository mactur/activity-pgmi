<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
  <meta charset="utf-8">
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Admin | @yield('title')</title>

  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="{{asset('assets/css/bootstrap.min.css')}}">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
  <!-- Ionicons -->
  <!-- Theme style -->
  <link rel="stylesheet" href="{{asset('assets/css/AdminLTE.min.css')}}">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="{{asset('assets/css/skins/_all-skins.min.css')}}">
  <link rel="stylesheet" href="{{asset('assets/plugins/morris/morris.css')}}">
  <link rel="stylesheet" href="{{asset('assets/css/style.css')}}">
  <link href="{{asset('assets/plugins/datatables/jquery.dataTables.min.css')}}" rel="stylesheet">
  <!-- Morris chart -->
  <!-- bootstrap wysihtml5 - text editor -->
  <link href="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.8/summernote.css" rel="stylesheet">
  <link rel="stylesheet" href="{{asset('assets/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css')}}">
  <link rel="stylesheet" href="{{asset('assets/plugins/datepicker/datepicker3.css')}}">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
    <style>
        .skin-blue .main-header .navbar{
        background-color:#29CC6D;
        }
        .skin-blue .main-header .logo{
          background-color: #1A7F44;
        }
        .box.box-primary{
            border-top-color: #1A7F44;
        }
        .skin-blue .sidebar-menu > li.active > a{
            border-left-color:#CCCA29
        }
        .skin-blue .wrapper, .skin-blue .main-sidebar, .skin-blue .left-side{
            background-color: #29cc6d;
        }
        .skin-blue .sidebar-menu > li:hover > a, .skin-blue .sidebar-menu > li.active > a, .skin-blue .sidebar-menu > li.menu-open > a{
            color: #fff;
            background-color: #84e789;
        }
        .skin-blue .sidebar a{
            color: #fff;

            font-weight: bold
        }
        .skin-blue .sidebar-form input[type="text"], .skin-blue .sidebar-form .btn{
            background-color: #fff;
            border-color:#fff;
        }
        .skin-blue .sidebar-menu > li > .treeview-menu{
            background-color: #1A7F44;
        }
    </style>
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

  <header class="main-header">
    <!-- Logo -->
    <a href="index2.html" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><b>PGMI</b></span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"><b>Admin</b>PGMI</span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a>

      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
          <li class="dropdown notifications-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <i class="fa fa-bell-o"></i>
              <span class="label label-warning">{{cBlog() > 0 ? cBlog() : ""}}</span>PEMBERITAHUAN
            </a>
            @if (cBlog() >= 1)

            <ul class="dropdown-menu">
              <li class="header">Kamu mempunyai {{cBlog()}} pemberitahuan</li>
              <li>
                <!-- inner menu: contains the actual data -->
                <ul class="menu">
                  @if(empty(rBlog()))
                @php $reader = rBlog();
                foreach($reader as $read) { @endphp
                  <li>
                    <a href="/adminn/edit-post/{{$read->id}}">
                      <i class="fa fa-users text-aqua"></i>{{$read->judul}}
                    </a>
                  </li>
                @php } @endphp
              @endif
                @if (!empty(rPres()))
                  @php $reader = rPres();
                  foreach($reader as $read) { @endphp
                    <li>
                      <a href="/adminn/edit-post/{{$read->id}}">
                        <i class="fa fa-users text-aqua"></i>{{$read->judul}}
                      </a>
                    </li>
                  @php } @endphp
                @endif
                </ul>
              </li>
              <li class="footer"><a href="/adminn/confirm-post/post">Lihat semua post</a></li>
              <li class="footer"><a href="/adminn/confirm-post/prestasi">Lihat semua prestasi</a></li>
            </ul>
            @endif
          </li>
          <!-- Control Sidebar Toggle Button -->
        </ul>
      </div>
    </nav>
  </header>
  <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- search form -->
      <!-- /.search form -->
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu" data-widget="tree" style="margin-top:30px">
        <li class="active">
          <a href="/adminn">
            <i class="fa fa-dashboard"></i> <span>Dashboard</span>
          </a>
        </li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-files-o"></i>
            <span>Posts</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="/adminn/add-post"><i class="fa fa-circle-o"></i> Tambah Post</a></li>
            <li><a href="{{route('all')}}"><i class="fa fa-circle-o"></i> Semua Post</a></li>
          </ul>
        </li>
        <li>
          <a href="{{route('slider')}}">
            <i class="fa fa-newspaper-o" aria-hidden="true"></i>
            <span>Slider Posts</span>
          </a>
        </li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-files-o"></i>
            <span>Persetujuan</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
          <li>
            <a href="/adminn/confirm-post/post">
              <i class="fa fa-check-square-o" aria-hidden="true"></i>
              <span>Persetujuan Posts</span>
            </a>
          </li>
          <li>
            <a href="/adminn/confirm-post/prestasi">
              <i class="fa fa-check-square-o" aria-hidden="true"></i>
              <span>Persetujuan Prestasi</span>
            </a>
          </li>
        </ul>
        </li>
        <li>
          <li>
            <a href="{{route('Prestasi')}}">
              <i class="fa fa-star"></i> <span>Prestasi</span>
            </a>
          </li>
        </li>
        <li><a href="{{ route('logout') }}" onclick="event.preventDefault();
                 document.getElementById('logout-form').submit();"><i class="fa fa-sign-out" aria-hidden="true"></i><span>Logout</span></a>
                 <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                     {{ csrf_field() }}
                 </form>
        </li>
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper" style="background-color:#fff;overflow:auto">

    <!-- Main content -->
    <section class="content">
        @yield('content')

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <footer class="main-footer">
    <strong>Copyright &copy; <?php print date("Y") ?><a href="http://justfriend-dev.com" style="color:#1A7F44; ">JustFriend Developer</a>.</strong> PGMI UIN Sunan Kalijaga
  </footer>

</div>
<!-- ./wrapper -->
<!-- jQuery 3.1.1 -->
<script src="{{asset('assets/plugins/jQuery/jquery-3.1.1.min.js')}}"></script>
<!-- jQuery UI 1.11.4 -->
<script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button);
</script>
<!-- Bootstrap 3.3.6 -->
<script src="{{asset('assets/js/bootstrap.min.js')}}"></script>
<!-- Morris.js charts -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
<script src="{{asset('assets/plugins/morris/morris.min.js')}}"></script>
<!-- Sparkline -->
<script src="{{asset('assets/plugins/sparkline/jquery.sparkline.min.js')}}"></script>
<!-- jvectormap -->
<script src="{{asset('assets/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js')}}"></script>
<script src="{{asset('assets/plugins/jvectormap/jquery-jvectormap-world-mill-en.js')}}"></script>
<!-- jQuery Knob Chart -->
<script src="{{asset('assets/plugins/knob/jquery.knob.js')}}"></script>
<!-- daterangepicker -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.11.2/moment.min.js"></script>
<script src="{{asset('assets/plugins/daterangepicker/daterangepicker.js')}}"></script>
<!-- datepicker -->
<script src="{{asset('assets/plugins/datepicker/bootstrap-datepicker.js')}}"></script>
<!-- Bootstrap WYSIHTML5 -->
<script src="{{asset('assets/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js')}}"></script>
<!-- Slimscroll -->
<script src="{{asset('assets/plugins/slimScroll/jquery.slimscroll.min.js')}}"></script>
<!-- FastClick -->
<script src="{{asset('assets/plugins/fastclick/fastclick.js')}}"></script>
<!-- AdminLTE App -->
<script src="{{asset('assets/js/adminlte.min.js')}}"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<script src="{{asset('assets/js/dashboard.js')}}"></script>
<script src="https://js.pusher.com/4.1/pusher.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/laravel-echo/dist/echo.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="{{asset('assets/js/demo.js')}}"></script>
<script src="{{asset('assets/plugins/datatables/moment.js')}}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.8/summernote.min.js"></script>
@section('js')

@show
<script>
$(document).ready( function() {
  $("#editor1").summernote({
    height : 150,
    tabsize: 2,
    toolbar: [
    // [groupName, [list of button]]
    ['style', ['style','undo','redo','bold', 'italic', 'underline','fontname',]],
    ['font', ['fontsize','strikethrough', 'superscript', 'subscript']],
    ['table', ['table']],
    ['color', ['color']],
    ['para', ['ul', 'ol', 'paragraph']],
    ['height', ['height']],
    ['insert', ['picture', 'link', 'hr', 'codeview','fullscreen']]
  ]});
});
$(document).ready( function() {
  $("#editor2").summernote({
    height : 150,
    tabsize: 2,
    toolbar: [
    // [groupName, [list of button]]
    ['style', ['style','undo','redo','bold', 'italic', 'underline','fontname',]],
    ['font', ['fontsize','strikethrough', 'superscript', 'subscript']],
    ['table', ['table']],
    ['color', ['color']],
    ['para', ['ul', 'ol', 'paragraph']],
    ['height', ['height']],
    ['insert', ['picture', 'link', 'hr', 'codeview','fullscreen']]
  ]});
});
</script>
<script type="text/javascript">
$(window).on('hashchange', function() {
    if (window.location.hash) {
        var page = window.location.hash.replace('#', '');
        if (page == Number.NaN || page <= 0) {
            return false;
        } else {
            getData(page);
        }
    }
});

$(document).ready(function() {
    $(document).on('click', '.pagination a',function(event) {
        $('li').removeClass('active');
        $(this).parent('li').addClass('active');
        event.preventDefault();

        var myurl = $(this).attr('href');
        var page=$(this).attr('href').split('page=')[1];

        getData(page);
    });
});

function getData(page) {
        $.ajax({
            url: '?page=' + page,
            type: "get",
            datatype: "html",
        }).done(function(data) {

            $(".post-all").empty().html(data);
            location.hash = page;
        })
        .fail(function(jqXHR, ajaxOptions, thrownError)
        {
              alert('No response from server');
        });
}
</script>

</body>
</html>
