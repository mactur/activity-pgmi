@section('js')
<script type="text/javascript" src="{{asset('assets/plugins/datatables/jquery.dataTables.min.js')}}"></script>
<script>
  var oTable;
  $(function () {
    oTable = $("#all-post").DataTable({
        dom: "<'row'<'col-xs-12'<'col-xs-6'l><'col-xs-6'p>>r>"+
            "<'row'<'col-xs-12't>>"+
            "<'row'<'col-xs-12'<'col-xs-6'i><'col-xs-6'p>>>",
        processing: true,
        serverSide: true,
        ajax: {
            url: '{{ url("/dataAll-post/adm") }}',
            data: function (d) {
                d.bahasa = $('input[name=bahasa]:checked').val();
                d.kategori = $('input[name=kategori]:checked').val();
            }
        },
        "fnCreatedRow": function (row, data, index) {
			$('td', row).eq(0).html(index + 1); },
        columns: [
        {data: 'id', name: 'id'},
        {data: 'judul', name: 'judul'},
        {data: 'created_at'},
        {data: 'nama', name: 'nama', searchable: false,},
        {data: 'id', orderable: false, searchable: false, 'mRender': function(data) {
            return '{{csrf_field()}}<button onClick="edit('+data+')" class="btn btn-primary btn-edit"><i class="fa fa-pencil" aria-hidden="true"></i></button> <button onClick="hapus('+data+')" class="btn btn-primary btn-edit"><i class="fa fa-trash" aria-hidden="true"></i></button>';
        }}, ],
        columnDefs: [
          { targets: 2, render:function(data){
            return moment(data).format('DD-MM-YYYY'); }},
        ]
    });
    $('.searchAjx').click(function(e) {
        oTable.draw();
        e.preventDefault();
    });

  });
    function edit(post) {
      window.location.href="/adminn/edit-post/"+post;
    }
    function hapus(post) {
      window.location.href="/adminn/hapus-post/"+post;
      /*if (confirm('yakin ingin dihapus?')) {
        $.ajax({
            url : '/adminn/destroy-post/',
            type : 'post',
            data : {'hapus': post,'_method':$('input[name=_method]').val(),'_token':$('input[name=_token]').val(),'hps':$('input[name=hps]').val()},
            success: function(data) {
                oTable.draw();
            }
        });
      }*/
  }
</script>
@stop
@extends('admin.admin')
@section('title', 'Semua Post')

@section('content')
<div class="box box-info" style="border-top-color: #e9ef00;box-shadow:none;margin-top:0px;padding-bottom:20px">
    <div class="col-sm-12">
      <div class="box" style="padding-bottom:10px;border:none;box-shadow:none">
        <div class="box-header" style="padding-left:0px">
          <h1 class="box-title" style="display:block;font-weight:bold;font-size:2.3em">SEMUA POST</h1>
            <div class="underscore" style="margin-left:0px;margin-left:0px;margin-bottom:15px;"></div>
            <label><b>Pencarian Menurut :</b></label>
            {!! Form::open(['url'=>'','method'=>'get']) !!}
            {{csrf_field()}}

            <div class="form-group" >
                <label class="radio-inline" style="cursor:text;padding-left:0px">Pilih Kategori<sup style="color:red">*</sup> :</label>
                @foreach ($category as $cat)
                    <label class="radio-inline">{{ Form::radio('kategori', $cat->id_kategori) }}{{$cat->nama}}</label>
                @endforeach
            </div>
            <div class="form-group">
                <label class="radio-inline" style="cursor:text;padding-left:0px">Pilih Bahasa<sup style="color:red">*</sup> :</label>
                <label class="radio-inline">{{ Form::radio('bahasa', 0) }}Bahasa Indonesia {{old('bahasa')}}</label>
                <label class="radio-inline">{{ Form::radio('bahasa', 1) }}Bahasa Inggris</label>
            </div>
            <button class="btn btn-info searchAjx" name="tombol" value="kliked" type="button"><i class="fa fa-search" aria-hidden="true" style="margin-right:5px"></i><b>Search</b></button>
            <input class="btn btn-danger" type="reset" name="reset" value="Reset">
            {!! Form::close() !!}
          <div class="box-tools">
            <div class="input-group input-group-sm" style="width: 150px;">
              <button type="button" class="btn btn-default" onClick="window.location.href='add-post'"><i class="glyphicon glyphicon-plus" style="margin-right:5px;"></i>Tambah Post</button>
            </div>
          </div>
        </div>
        @if ($message = Session::has('message'))
        <div class="alert alert-success sukses">
            <button type="button" class="close" data-dismiss="alert">×</button>
                <strong>{{$message}}</strong>
        </div>
        @endif
        <!-- /.box-header -->
        <div class="post-all">
            @include('admin.data-all-post')
        </div>

        <!-- /.box-body -->
      </div>
      <!-- /.box -->
    </div>


</div>
@endsection
