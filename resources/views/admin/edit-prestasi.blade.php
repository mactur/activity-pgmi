@section('js')
<script type="text/javascript">
$(document).ready( function() {
  $("#editor1").summernote({
    height : 150,
    tabsize: 2,
    toolbar: [
    // [groupName, [list of button]]
    ['style', ['style','undo','redo','bold', 'italic', 'underline','fontname',]],
    ['font', ['fontsize','strikethrough', 'superscript', 'subscript']],
    ['table', ['table']],
    ['color', ['color']],
    ['para', ['ul', 'ol', 'paragraph']],
    ['height', ['height']],
    ['insert', ['picture', 'link', 'hr', 'codeview','fullscreen']]
  ]});
});
$(document).ready( function() {
  $("#editor2").summernote({
    height : 150,
    tabsize: 2,
    toolbar: [
    // [groupName, [list of button]]
    ['style', ['style','undo','redo','bold', 'italic', 'underline','fontname',]],
    ['font', ['fontsize','strikethrough', 'superscript', 'subscript']],
    ['table', ['table']],
    ['color', ['color']],
    ['para', ['ul', 'ol', 'paragraph']],
    ['height', ['height']],
    ['insert', ['picture', 'link', 'hr', 'codeview','fullscreen']]
  ]});
});
      function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#showgambar').attr('src', e.target.result);
            }

            reader.readAsDataURL(input.files[0]);
        }
    }

    $("#gambar").change(function () {
        readURL(this);
    });
    function confirmed(post) {
    $.ajax({
        url : '/adminn/confirmed/'+post,
        type : 'get',
        data : {'data': post,'page':'prestasi'},
        success: function(data) {
            history.go(-1);
        }
    });
  }
</script>
@stop
@extends('admin.admin')
@section('title', 'Edit Prestasi')

@section('content')
  <div class="box box-info" style="border-top-color: #e9ef00;box-shadow:none;margin-top:0px;padding-bottom:20px">
      <div class="col-sm-12" style="margin-top:15px;">
         <pstyle="display:inline-block;"><a href="/adminn/">Home</a></p><span style="margin-left:5px;margin-right:5px;">>></span><p style="display:inline-block"><a href="#">Edit Prestasi</a></p>
      </div>
      {!! Form::open(array('route'=>'ePrestasi','files'=>true)) !!}
        <input type="hidden" name="data" value="{{$tampil[0]['id']}}">
        <input type="hidden" name="_method" value="PUT">
        {{csrf_field()}}
         <div class="col-md-6">
                 <h3>EDIT POST PRESTASI</h3>
                 <div class="underscore" style="margin-left:0px;margin-left:0px;margin-bottom:15px;"></div>
                 <div class="form-group">
                         <label>Nama Lomba / Kegiatan<sup style="color:red">*</sup></label>
                         <input type="text" class="form-control" id="lombaPost" placeholder="Nama Lomba / Kegiatan" required name="judul" value="{{$tampil[0]['judul']}}">
                 </div>
                 <div class="box-body pad" style="padding:0px">
                     <label for="deskripsiLomba">Deskripsi Lomba / Kegiatan</label>

                                 <textarea id="editor1" name="content" rows="10" cols="80">
                                   {{$tampil[0]['content']}}
                                 </textarea>

                 </div>
         </div>
         <div class="col-md-6" style="padding-top: 75px">
           <div class="form-group">
                   <label>Nama Peserta Lomba / Kegiatan<sup style="color:red">*</sup></label>
                   <input type="text" class="form-control" id="namaPeserta" name="peserta" required value="{{$tampil[0]['peserta']}}" placeholder="Nama">
           </div>
            <div class="form-group">
               <label>Sifat Lomba / Kegiatan<sup style="color:red">*</sup></label>
               <select class="form-control" name="sifat" required>
                   <option value="{{$tampil[0]['sifat']}}">{{$tampil[0]['sifat']}}</option>
                   <option value="Group">Group</option>
                   <option value="Individu">Individu</option>
               </select>
           </div>
           <div class="form-group">
               <label>Jenjang Lomba / Kegiatan<sup style="color:red">*</sup></label>
               <select class="form-control" name="jenjang" id="sel1" required>
                   <option value="{{$tampil[0]['jenjang']}}">{{$tampil[0]['jenjang']}}</option>
                   <option value="Kecamatan">Kecamatan</option>
                   <option value="Kabupaten">Kabupaten</option>
                   <option value="Provinsi">Provinsi</option>
                   <option value="Nasional">Nasional</option>
                   <option value="Internasional">Internasional</option>
               </select>
           </div>
           <div class="form-group">
               <label style="display:block">Foto Kegiatan<sup style="color:red">*</sup></label>
               <img src="{{asset('assets/img/blogs/'.$tampil[0]['gambar'])}}" id="showgambar" style="max-width:200px;max-height:200px;float:left;" />
               <input type="file" id="gambar" name="gBlog" accept="image/*" data-type='image'><br>
           </div>
           <div class="form-group">
               <label style="display:block">Tanggal Lomba<sup style="color:red">*</sup></label>
               <input type="date" class="form-control" name="tanggal" value="{{$tampil[0]['tanggal']}}">
           </div>
         </div>

   </div>
   <div class="col-sm-12" style="margin-bottom:15px;">
     <input type="submit" style="margin-top:20px" class="btn btn-success" value="Submit">
     <button type="reset" style="margin-top:20px" class="btn btn-danger">Reset</button>
     @if ($tampil[0]['aktif'] != 1)
       <button style='margin-top:20px' onClick="confirmed({{$tampil[0]['id']}})" class='btn btn-danger'>Setujui</button>
   @endif
   </div>
   {!!Form::close()!!}
@endsection
