@section('js')
<script type="text/javascript">

      function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#showgambar').attr('src', e.target.result);
            }

            reader.readAsDataURL(input.files[0]);
        }
    }

    $("#gambar").change(function () {
        readURL(this);
    });

</script>

@stop
@extends('admin.admin')
@section('title', 'Tambah Slider')

@section('content')
<div class="box box-info" style="border-top-color: #e9ef00;box-shadow:none;margin-top:0px;padding-bottom:20px">
    <div class="col-sm-12" style="margin-top:15px;">
       <p  style="display:inline-block;"><a href="#">Home</a></p><span style="margin-left:5px;margin-right:5px;">>></span><p style="display:inline-block"><a href="#">Tambah Slider</a></p>
    </div>
    {!! Form::open(array('route'=>'SimpanSlider','files'=>true)) !!}
       <div class="col-md-6">
           <div class="box-header">
               <h3>TAMBAH SLIDER</h3>
               <div class="underscore" style="margin-left:0px;margin-left:0px;margin-bottom:15px;"></div>
               <div class="form-group">
                  <label for="captionId">Caption Indonesia<sup style="color:red">*</sup></label>

                  <input type="text" value="{{old('captionId')}}" required class="form-control" name="captionId" id="cptId" placeholder="Caption Bahasa Indonesia">
               </div>
               <div class="form-group">
                  <label for="judulPost">Caption English<sup style="color:red">*</sup></label>

                  <input type="text" value="{{old('captionEn')}}" required class="form-control" name="captionEn" id="cptEn" placeholder="Caption Bahasa English">
               </div>
               <div class="form-group{{ $errors->has('gambarCaption') ? ' has-error' : '' }}col-md-12">
                  <label for="imgCaption" style="display:block">Gambar Caption<sup style="color:red">*</sup></label>
                  <img src="" id="showgambar" style="max-width:200px;max-height:200px;float:left;" /><br />
                  <input type="file" required id="gambar" name="gambarCaption" accept="image/*" data-type='image'><br>
                  @if ($errors->has('gambarCaption'))
                      <span class="help-block">
                          <strong>{{ $errors->first('gambarCaption') }}</strong>
                      </span>
                  @endif
               </div>
               <div class="form-group col-md-12">
                  <input type="submit" value="Submit" id='simpan' name="simpan" style="margin-top:20px" class="btn btn-success">
                  <button style="margin-top:20px" class="btn btn-danger">Batal</button>
               </div>
           </div>
       </div>
 </div>
{{csrf_field()}}
{!! Form::close() !!}
@endsection
