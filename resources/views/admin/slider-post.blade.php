@section('js')
  <script type="text/javascript">
  function destroy(post) {
    $.ajax({
        url : '/adminn/destroy-slider/',
        type : 'post',
        data : {'hapus': post,'_method':$('input[name=_method]').val(),'_token':$('input[name=_token]').val()},
        success: function(data) {
            $('.post-all').empty().html(data);
            $('.sukses').show();
            $('#message').html('Slider Berhasil dihapus');
        }
    });
  }
  </script>
@stop
@extends('admin.admin')
@section('title', 'Slider')
@section('content')
<div class="box box-info" style="border-top-color: #e9ef00;box-shadow:none;margin-top:0px;padding-bottom:20px">
    <div class="col-sm-12">
      <div class="box" style="padding-bottom:10px;border:none;box-shadow:none">
        <div class="box-header" style="padding-left:0px">
          <h1 class="box-title" style="display:block;font-weight:bold;font-size:2.3em">SLIDER POST</h1>
            <div class="underscore" style="margin-left:0px;margin-left:0px;margin-bottom:15px;"></div>
          <div class="box-tools">
            <div class="input-group input-group-sm" style="width: 150px;">
              <button type="button" onclick="window.location.href='add-slider'" class="btn btn-default">
                  <i class="glyphicon glyphicon-plus" style="margin-right:5px;"></i>Tambah Slider Post</button>
            </div>
          </div>
        </div>
        <div class="alert alert-success sukses" hidden="hidden">
            <button type="button" class="close" data-dismiss="alert">×</button>
              <strong id="message"></strong>
        </div>
        <!-- /.box-header -->
        <div class="post-all">
          @include('admin.data-slider-post')
        </div>
        <!-- /.box-body -->
      </div>
      <!-- /.box -->
    </div>
</div>
@endsection
