@extends('layouts.master')
@section('title', $blogs[0]['nama']? $blogs[0][$bhs == 'id' ? 'nama' : 'namaEn'] : 'Not Found')
@section('content')
<div id="list-post-wrap">
    <div class="container">
      <div class="col-md-9">
          <div id="list-into">
            <p class="list-page"><a href="/{{$bhs}}">{{$navs[0]['nav']}}</a></p>
            @if (!empty($blogs[0]['judul']))
              >><p class="list-page"><a href="/{{$bhs}}/{{$url}}">{{$blogs[0][$bhs == 'id' ? 'nama' : 'namaEn']}}</a></p>
            @endif
          </div>
          <div id="title-list-posts-wrap">
          @if (!empty($blogs[0]['judul']))
              <h2 class="title-section" style="text-align:left">{{$blogs[0][$bhs == 'id' ? 'nama' : 'namaEn']}}</h2>
          @else
            <h2 class="title-section" style="text-align:left">{{$bhs == 'id' ? 'Kosong' : 'Not Found'}}</h2>
        @endif
              <div class="underscore" style="margin-left:0px;margin-right:0px;"></div>
          </div>
          <div class="blogss">
          @if(empty($blogs[0]['judul']) and $bhs == 'id')
            <h3>Maaf Halaman yang anda cari belum ada</h3>
          @elseif(empty($blogs[0]['judul']) and $bhs == 'en')
            <h3>Sorry this page your search empty</h3>
          @endif
              @include('layouts.dataBlogs')
          </div>
      </div>
      <div class="col-md-3">
          @include('layouts.sidebar')
      </div>
    </div>
</div>
@endsection
