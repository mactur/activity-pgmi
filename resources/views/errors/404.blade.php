
<head>
    <meta charset="utf-8">
    <title>Not Found</title>
    <style>
        .error-template {padding: 40px 15px;text-align: center;}
        .error-actions {margin-top:15px;margin-bottom:15px;}
        .error-actions .btn { margin-right:10px; }
    </style>
    <!-- Bootstrap -->
    <link href="{{asset('assets/css/bootstrap.min.css')}}" rel="stylesheet">
</head>
<div class="container">
    <div class="row">
        <div class="error-template">
            <h1>Oops!</h1>
            <h2>404 Not Found</h2>
            <div class="error-details">
            Maaf, Halaman yang anda cari tidak ada<br>

            </div>
            <div class="error-actions">
            <button onClick="return history.go(-1)" class="btn btn-primary">
                <i class="icon-home icon-white"></i> Kembali </button>
            <a href="pgmi@uin-suka.ac.id" class="btn btn-default">
                <i class="icon-envelope"></i> Contact Support </a>
            </div>
        </div>
    </div>
</div>
